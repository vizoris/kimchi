$(function() {

// MMENU
if ($(window).width() < 1200) {
var menu = new MmenuLight(
  document.querySelector( '#mobile-menu' ),
  'all'
);

var navigator = menu.navigation({
  // selectedClass: 'Selected',
  // slidingSubmenus: true,
  // theme: 'dark',
   title: 'Меню'
});

var drawer = menu.offcanvas({
  // position: 'left'
});

//  Open the menu.
document.querySelector( 'a[href="#mobile-menu"]' )
  .addEventListener( 'click', evnt => {
    evnt.preventDefault();
    drawer.open();
});


}


// Фиксированная шапка припрокрутке
$(window).scroll(function(){

  if ($(window).width() > 768) {
      var sticky = $('.header'),
          scroll = $(window).scrollTop();
      if (scroll > 100) {
          sticky.addClass('header-fixed');
      } else {
          sticky.removeClass('header-fixed');
      };
    }

   
});


$(window).resize(function() {
  if ($(window).width() <= 768) {
      $('.header').removeClass('header-fixed'); 
    }

});



// Выпадающее меню
  if ($(window).width() > 991) {
    $('.drop').hover(function() {
      $(this).children('.drop-menu').stop(true, true).toggleClass('active');
    })
}

// Banner slider
  $('.banner').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      // autoplay: true,
      // autoplaySpeed:4000,
      fade: false,
      arrows: false,
      dots: true,
      responsive: [
        {
          breakpoint: 991,
          settings: {
            dots: false
          }
        },
      ]
     
  });



// Счетчик при клике "Добавить в корзину"
$('.product-item__bottom--btn .btn').click(function() {
  $(this).fadeOut(0);
  $(this).next('.count').addClass('active');
})




// Календарь
$(function () {
    $('#datetimepicker').datetimepicker({
        locale: 'ru',
        format: 'DD/MM/YYYY'

    });
});



// FAQ
$('.faq-item__header').click(function(){
  if(!($(this).next().hasClass('active'))){
    $('.faq-item__body').slideUp().removeClass('active');
    $(this).next().slideDown().addClass('active');
  }else{
    $('.faq-item__body').slideUp().removeClass('active');
  };

  if(!($(this).parent('.faq-item').hasClass('active'))){
    $('.faq-item').removeClass("active");
    $(this).parent('.faq-item').addClass("active");
  }else{
    $(this).parent('.faq-item').removeClass("active");
  };
});


 // Стилизация селектов
$('select').styler();




// Плейсфолдер в инпутах
const FloatLabel = (() => {
  
  // add active class and placeholder 
  const handleFocus = (e) => {
    const target = e.target;
    target.parentNode.classList.add('active');
    
  };
  
  // remove active class and placeholder
  const handleBlur = (e) => {
    const target = e.target;
    if(!target.value) {
      target.parentNode.classList.remove('active');
    }
     
  };  
  
  // register events
  const bindEvents = (element) => {
    const floatField = element.querySelector('input,textarea');
    floatField.addEventListener('focus', handleFocus);
    floatField.addEventListener('blur', handleBlur);    
  };
  
  // get DOM elements
  const init = () => {
    const floatContainers = document.querySelectorAll('.form-item');
    
    floatContainers.forEach((element) => {
      if (element.querySelector('input,textarea').value) {
          element.classList.add('active');
      }      
      
      bindEvents(element);
    });
  };
  
  return {
    init: init
  };
})();

FloatLabel.init();



// Показать/скрыть корзину при наведении
$('.header-cart').hover(function() {
  $(this).toggleClass('active');
  $('.cart-block').stop(true, true).fadeToggle(200);
})


// FansyBox
 $('.fancybox').fancybox({
  closeExisting: true,
 });


// Маска телефона
$(".phone-mask").mask("+7 (999) 99-99-999");

// Маска кода
$(".code-mask").mask("9 9 9 9");


// Календарь
$(function () {
    $('#datepicker').datetimepicker({
        locale: 'ru',
        format: 'DD/MM/YYYY'

    });
});
// Время доставки часы
$(function () {
    $('#timepicker').datetimepicker({
        locale: 'ru',
        format: 'HH'

    });
});

// Время доставки минуты
$(function () {
    $('#minutepicker').datetimepicker({
        locale: 'ru',
        format: 'mm'

    });
});



// Меню в футере на мобильном
if ($(window).width() <= 768) {
  $('.footer-links__item .h5').click(function() {
    console.log('click')
    $(this).next('ul').fadeToggle();
    $(this).toggleClass('active');
  })

}



// Переключатель
$('.switch-btn').click(function (e, changeState) {
    if (changeState === undefined) {
        $(this).toggleClass('switch-on');
    }
    if ($(this).hasClass('switch-on')) {
        $(this).trigger('on.switch');
    } else {
        $(this).trigger('off.switch');
    }
});



// Begin of slider-range
$( "#slider-range" ).slider({
  range: 'min',
  min: 0,
  max: 500,
  value: 150,
  slide: function( event, ui ) {
    $( "#amount" ).text(ui.value);
  }
});
$( "#amount" ).val( $( "#slider-range" ).slider( "value" ) );

// END of slider-range



// Табы
$('.tabs-nav a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $(this).parents('.tabs-wrap').find('#' + _id);
    $(this).parents('.tabs-nav').find('a').removeClass('active');
    $(this).addClass('active');
     $(this).parents('.tabs-wrap').find('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});





// establishments-slider
  $('.establishments-slider').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed:4000,
      arrows: false,
      dots: true,
      // variableWidth: true,

      responsive: [
      {
        breakpoint: 1200,
        settings: {
            slidesToShow: 2,
        }
        
      },
      {
        breakpoint: 768,
        settings: {
            slidesToShow: 1,
        }
        
      },
    ]
     
  });






// establishments-slider
  $('.feedbacks-slider').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed:4000,
      arrows: false,
      dots: true,

     
  });


















































})